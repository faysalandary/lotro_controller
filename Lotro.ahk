JoyMultiplier = 0.75
JoyThreshold = 10
InvertYAxis := false

#SingleInstance
#Persistent
#NoEnv 
SendMode Input 
JoystickPrefix = Joy

JoyThresholdUpper := 50 + JoyThreshold
JoyThresholdLower := 50 - JoyThreshold
if InvertYAxis
	YAxisMultiplier = -1
else
	YAxisMultiplier = 1

SetTimer, WatchJoystick, 10 
SetTimer, WatchPOV, 5

GetKeyState, JoyInfo, %JoystickNumber%JoyInfo

return  

WatchJoystick:
MouseNeedsToBeMoved := false
SetFormat, float, 03
GetKeyState, joyu, %JoystickNumber%joyu
GetKeyState, joyr, %JoystickNumber%joyr
GetKeyState, joyz, %JoystickNumber%joyz

if joyu > %JoyThresholdUpper%
{
	MouseNeedsToBeMoved := true
	DeltaX := joyu - JoyThresholdUpper
}
else if joyu < %JoyThresholdLower%
{
	MouseNeedsToBeMoved := true
	DeltaX := joyu - JoyThresholdLower
}
else
	DeltaX = 0
if joyr > %JoyThresholdUpper%
{
	MouseNeedsToBeMoved := true
	DeltaY := joyr - JoyThresholdUpper
}
else if joyr < %JoyThresholdLower%
{
	MouseNeedsToBeMoved := true
	DeltaY := joyr - JoyThresholdLower
}
else
	DeltaY = 0
if MouseNeedsToBeMoved
{
	SetMouseDelay, -1  
	MouseMove, DeltaX * JoyMultiplier, DeltaY * JoyMultiplier * YAxisMultiplier, 0, R
}

if( joyz < 25 and not GetKeyState("x","P")){
	  Send {x down}
          SetTimer, WaitForJoyXUp, 10
}
else if( joyz > 75 and not GetKeyState("Control","P")){
	  Send {Control down}
          SetTimer, WaitForJoyzUp, 10
}

joy1::Send 6
joy2::Send 7
joy3::Send 5
joy4::Send 8
joy7::Send y
joy8::Send m

joy5::
  Send {Alt down}
  SetTimer, WaitForJoy5Up, 10
return

joy10::
  Click down
  SetTimer, WaitForJoy10Up, 10
return

joy6::
  Click down Right
  SetTimer, WaitForJoy6Up, 10
return

joy9::
  Send {Shift down}
  SetTimer, WaitForJoy9Up, 10
return

WatchPOV:
GetKeyState, POV, JoyPOV  
KeyToHoldDownPrev = %KeyToHoldDown%  

if POV < 0   ; No angle to report
    KeyToHoldDown =
else if POV > 31500                
    KeyToHoldDown = 4              
else if POV between 0 and 4500     
    KeyToHoldDown = 4              
else if POV between 4501 and 13500 
    KeyToHoldDown = 3              
else if POV between 13501 and 22500
    KeyToHoldDown = 2              
else                               
    KeyToHoldDown = 1              

if KeyToHoldDown = %KeyToHoldDownPrev%  
    return  

SetKeyDelay -1  
if KeyToHoldDownPrev 
    Send, {%KeyToHoldDownPrev% up} 
if KeyToHoldDown 
    Send, {%KeyToHoldDown% down}
return

WaitForJoy5Up:
if GetKeyState("joy5")
    return

WaitForJoy6Up:
if GetKeyState("joy6")
    return
	
WaitForJoy9Up:
if GetKeyState("joy9")
    return

WaitForJoy10Up:
if GetKeyState("joy10")
    return

WaitForJoyXUp:
if( joyz < 25 )
  return

WaitForJoyzUp:
if( joyz > 75 )
  return

Send {Alt up}
Send {Shift up}
Click up
Click up Right
Send {Control up}
Send {x up}
SetTimer, WaitForJoyzUp, off
SetTimer, WaitForJoy6Up, off
SetTimer, WaitForJoy5Up, off
SetTimer, WaitForJoy9Up, off
SetTimer, WaitForJoy10Up, off
SetTimer, WaitForJoyXUp, off
return